(function (angular) {

    var cellars = angular.module('cellars', [
        'ngResource',
        'ngSanitize'
    ]);

    cellars.provider('tokenStore', function () {

        this.$get = ['$log', function ($log) {
            return {
                getToken: function () {
                    return window.localStorage.getItem('_cellars_token');
                },
                setToken: function (token) {
                    $log.debug('Saving token in local storage');
                    window.localStorage.setItem('_cellars_token', token);
                },
                clearToken: function () {
                    $log.debug('Removing token from local storage');
                    window.localStorage.removeItem('_cellars_token');
                }
            };

        }];
    });

    cellars.provider('authInterceptor', function () {

        this.$get = ['$rootScope', '$q', 'tokenStore', '$location', '$log', 'CellarsSDK', function ($rootScope, $q, tokenStore, $location, $log, CellarsSDK) {
            return {
                // Add authorization token to headers
                request: function (config) {
                    config.headers = config.headers || {};
                    if (tokenStore.getToken() && config.url.indexOf('localhost') === -1) {
                        CellarsSDK.auth = { token: tokenStore.getToken() };
                        config.headers['X-App-Key'] = CellarsSDK.http.app_key;
                        config.headers.Authorization = 'Bearer ' + tokenStore.getToken();
                    }
                    return config;
                },

                // Intercept 401s and redirect you to login
                responseError: function (response) {

                    if (response.status === 401) {
                        $log.info('Unauthorized:', response.config.url);

                        // remove any stale tokens
                        tokenStore.clearToken();
                        $location.path('/login');
                        return $q.resolve(response.data);
                    }
                    else if (response.status === 400) {
                        return $q.resolve(response.data);
                    }
                    else {
                        $log.error('Server Error', JSON.stringify(response));
                        return $q.reject(response);
                    }
                }
            };
        }];

    });

    /**
     * Expose the underlying cellars sdk for configuration purpose
     */
    cellars.provider('CellarsSDK', function () {

        this.server_url = 'https://api.cellars.io';
        this.api_version = '1.0';
        this.unsafe = true;
        this.log_level = 2;
        this.no_stream = true;
        this.app_key = '69808220090f35737cdb6ab12803e583';
        this.quality_level = 8;
        this.force_remote = {};

        //NOTE: No read or write store by default

        this.$get = function () {
            console.log('Creating a platform instance pointing to %s with version %s', this.server_url, this.api_version);
            return new Cellars({
                url: this.server_url,
                unsafe: this.unsafe,
                no_stream: this.no_stream,
                log_level: this.log_level,
                app_key: this.app_key,
                api_version: this.api_version,
                quality_level: this.quality_level,
                read_store: this.read_store,
                write_store: this.write_store,
                force_remote: this.force_remote
            });

        };

    });

    cellars.provider('Search', function () {

        this.$get = ['CellarsSDK', function (CellarsSDK) {
            return {
                search: function (identity, options) {
                    return CellarsSDK.catalog.search(identity, options);
                }, //add new function to search in products_detail
                fullsearch: function (identity, options) {
                    return CellarsSDK.catalog.fullsearch(identity, options);
                }
            };
        }];
    });

    cellars.provider('Auth', function () {

        this.$get = ['$location', '$rootScope', '$http', 'tokenStore', '$q', '$log', 'CellarsSDK', function ($location, $rootScope, $http, tokenStore, $q, $log, CellarsSDK) {
            $log.debug('Initializing Auth service');

            var currentUser = P.pending();

            function _getCurrentUser() {
                console.log('Auth.getCurrentUser');
                if (!currentUser.promise.isFulfilled()) {
                    CellarsSDK.getCurrentUser().then(function (result) {
                        currentUser.resolve(result.data);

                        $rootScope.$broadcast('cellar-resolved-currentUser', result.data);

                        $rootScope.guest = result.data.groups.indexOf('guest') > -1;
                        $rootScope.superuser = result.data.groups.indexOf('superuser') > -1;
                        $rootScope.admin = result.data.groups.indexOf('admin') > -1;
                        $rootScope.professional = result.data.groups.indexOf('professional') > -1;
                        $rootScope.restaurant = result.data.groups.indexOf('restaurant') > -1;

                        return result.data;
                    }).catch(function (err) {
                        $log.error('Unable to retrieve currentUser. Let\'s logout!', err);
                        tokenStore.clearToken();
                        CellarsSDK.auth = {};

                        // Reset current user promise
                        currentUser = P.pending();

                        $rootScope.$broadcast('cellar-reset-currentUser', currentUser.promise);

                        $rootScope.guest = true;
                        $rootScope.superuser = false;
                        $rootScope.admin = false;
                        $rootScope.professional = false;
                        $rootScope.restaurant = false;
                    });
                }
                else {
                    var userData = currentUser.promise.value();
                    $rootScope.guest = userData.groups.indexOf('guest') > -1;
                    $rootScope.superuser = userData.groups.indexOf('superuser') > -1;
                    $rootScope.admin = userData.groups.indexOf('admin') > -1;
                    $rootScope.professional = userData.groups.indexOf('professional') > -1;
                    $rootScope.restaurant = userData.groups.indexOf('restaurant') > -1;
                }

                return currentUser.promise;
            }

            if (tokenStore.getToken()) {
                CellarsSDK.auth = { token: tokenStore.getToken() };
                _getCurrentUser();
            }

            return {

                /**
                 * Authenticate user and save token
                 *
                 * @param  {Object}   user     - login info
                 * @param  {Function} callback - optional
                 * @return {Promise}
                 */
                login: function (user, callback) {
                    var cb = callback || angular.noop;
                    var deferred = $q.defer();

                    $log.debug('Auth:login');
                    $log.debug('Auth:base_url:' + CellarsSDK.base_url);

                    CellarsSDK.authenticate(user).then(function (result) {
                        $log.debug('Received a token from platform');
                        tokenStore.setToken(result.token);
                        CellarsSDK.auth = { token: result.token };
                        _getCurrentUser();
                        $rootScope.guest = false;
                        deferred.resolve(result);
                        return cb();
                    }, function (err) {
                        $log.error(err);
                        this.logout();
                        deferred.reject(err);
                        return cb(err);
                    }.bind(this));

                    return deferred.promise;
                },

                /**
                 * Delete access token and user info
                 */
                logout: function () {
                    $log.debug('Auth:logout');
                    tokenStore.clearToken();
                    CellarsSDK.auth = {};

                    // Reset current user promise
                    currentUser = P.pending();

                    $rootScope.guest = true;
                    $rootScope.superuser = false;
                    $rootScope.admin = false;
                    $rootScope.professional = false;
                },

                /**
                 * Create a new user
                 *
                 * @param  {Object}   user     - user info
                 * @param  {Function} callback - optional
                 * @return {Promise}
                 */
                createUser: function (user, callback) {
                    var cb = callback || angular.noop;
                    return CellarsSDK.users.create(user).then(function (data) {
                        tokenStore.setToken(data.token);
                        CellarsSDK.auth = { token: data.token };
                        _getCurrentUser();
                        return cb(user);
                    });
                },

                /**
                 * Gets all available info on authenticated user
                 *
                 * @return {Object} user
                 */
                getCurrentUser: _getCurrentUser,
                getCurrentUserSync: function () {
                    if (currentUser.promise.isFulfilled()) {
                        return currentUser.promise.value();
                    }
                },

                /**
                 * Check if a user is logged in
                 *
                 * @return {Boolean}
                 */
                isLoggedIn: function () {
                    return typeof CellarsSDK.auth.token !== 'undefined';
                },

                /**
                 * Waits for currentUser to resolve before checking if user is logged in
                 */
                isLoggedInAsync: function (cb) {
                    return currentUser && currentUser.promise.isFulfilled();
                },

                /**
                 * Check if a user is an admin
                 *
                 * @return {Boolean}
                 */
                isAdmin: function () {
                    if (currentUser.promise.isFulfilled()) {
                        return currentUser.promise.value().groups.indexOf('admin') !== -1;
                    }
                },

                /**
                 * Check if a user is an admin
                 *
                 * @return {Boolean}
                 */
                isSommelier: function () {
                    if (currentUser.promise.isFulfilled()) {
                        return currentUser.promise.value().groups.indexOf('sommelier') !== -1;
                    }
                },

                /**
                 * Check if a user is a restaurant
                 *
                 * @return {Boolean}
                 */
                isRestaurant: function () {
                    if (currentUser.promise.isFulfilled()) {
                        return currentUser.promise.value().groups.indexOf('restaurant') !== -1;
                    }
                },

                hasGroup: function (group) {
                    if (currentUser.promise.isFulfilled()) {
                        return currentUser.promise.value().groups.indexOf(group) !== -1;
                    }
                },

                /**
                 * Get auth token
                 */
                getToken: function () {
                    return tokenStore.getToken();
                },

                /**
                 * Social Login: Create a new account or establish a platform token
                 * using a social provider token.
                 */
                socialLogin: function (provider, token, facebookAppId, lang, callback) {
                    var deferred = $q.defer();
                    var cb = callback || angular.noop;

                    $log.debug('Auth:socialLogin');
                    $log.debug('Auth:base_url:' + CellarsSDK.base_url);

                    CellarsSDK.social_connect(provider, token, facebookAppId, lang).then(function (result) {
                        $log.debug('Received a token from platform');
                        tokenStore.setToken(result.token);
                        CellarsSDK.auth = { token: result.token };
                        _getCurrentUser();
                        $rootScope.guest = false;
                        deferred.resolve(result);
                        return cb();
                    }, function (err) {
                        $log.error(err);
                        this.logout();
                        deferred.reject(err);
                        return cb(err);
                    }.bind(this));

                    return deferred.promise;
                },

                socialConnect: function (provider, token) {
                    return CellarsSDK.social.connect(provider, { token: token });
                }
            };

        }];
    });

    cellars.config(['$httpProvider', '$resourceProvider', function ($httpProvider, $resourceProvider) {
        $httpProvider.interceptors.push('authInterceptor');

        // Configure our resource provider
        $resourceProvider.defaults.stripTrailingSlashes = false;
    }]);

    /**
     * Winelist : Abstract winelist access and management methods
     */
    cellars.provider('Winelist', function () {

        this.$get = ['CellarsSDK', function (CellarsSDK) {
            return {
                create: function (winelist) {
                    return CellarsSDK.winelists.create(winelist);
                },
                update: function (winelist) {
                    return CellarsSDK.winelists.update(winelist);
                },
                rename: function (winelist) {
                    return CellarsSDK.winelists.rename(winelist.key, winelist.title);
                },
                list: function () {
                    return CellarsSDK.winelists.list();
                },
                render: function (winelistId, options) {
                    return CellarsSDK.winelists.render(winelistId, options);
                },
                addProducts: function (winelistId, AUPIDs) {
                    return CellarsSDK.winelists.addToList(winelistId, AUPIDs);
                },
                removeProduct: function (winelistId, AUPID) {
                    return CellarsSDK.winelists.removeFromList(winelistId, AUPID);
                },
                toggleProduct: function (winelistId, AUPID) {
                    return CellarsSDK.winelists.toggleProduct(winelistId, AUPID);
                },
                listProducts: function (winelistId, options) {
                    return CellarsSDK.winelists.render(winelistId, options);
                },
                isProductContained: function (AUPID, winelistId) {
                    return this.listProducts(winelistId).then(function (result) {
                        return !_.isUndefined(_.find(result.data, function (p) { return p.AUPID === AUPID; }));
                    }).catch(function () {
                        // We might have 401 errors in guest mode. Just return an empty array
                        return [];
                    });
                },
                getWinelist: function (winelistId) {
                    return CellarsSDK.winelists.load(winelistId);
                }
            };

        }];

    });

    cellars.provider('RequestNewProduct', function () {

        this.$get = ['CellarsSDK', function (CellarsSDK) {
            return {
                requestNewProduct: function (identity, aspects, options) {
                    return CellarsSDK.raw.requestNewProduct(identity, aspects || [], options || {});
                }
            };
        }];
    });

    cellars.provider('SellingPrice', function () {

        this.$get = ['CellarsSDK', function (CellarsSDK) {
            return {
                listRules: function () {
                    return CellarsSDK.sellingPrice.listRules();
                },
                updateRules: function (rules) {
                    return CellarsSDK.sellingPrice.updateRules(rules);
                },
                search: function (query) {
                    return CellarsSDK.sellingPrice.search(query);
                },
                update: function (aupid, data){
                    return CellarsSDK.sellingPrice.update(aupid, data);
                },
                applyRules: function(){
                    return CellarsSDK.sellingPrice.applyRules();
                } 
            };
        }];
    });

    cellars.provider('Cms', function () {

        this.$get = ['CellarsSDK', function (CellarsSDK) {
            return {
                create: function (key, spec, options) {
                    return CellarsSDK.cms.create(key, spec, options);
                },
                update: function (key, spec, options) {
                    return CellarsSDK.cms.update(key, spec, options);
                },
                list: function (query) {
                    return CellarsSDK.cms.list(query);
                },
                load: function (key, options) {
                    return CellarsSDK.cms.show(key, options);
                },
                remove: function (key) {
                    return CellarsSDK.cms.remove(key);
                },
                search: function (query) {
                    return CellarsSDK.cms.search(query);
                }
            };
        }];
    });

    cellars.provider('Crm', function () {

        this.$get = ['CellarsSDK', function (CellarsSDK) {
            return {
                showProfile: function (type, username) {
                    return CellarsSDK.crm.showProfile(type, username);
                },
                updateProfile: function (type, username, profile) {
                    return CellarsSDK.crm.updateProfile(type, username, profile);
                },
                removeProfile: function (type, username) {
                    return CellarsSDK.crm.removeProfile(type, username);
                },
                createProfile: function (type, username, profile) {
                    if (arguments.length === 2) {
                        profile = username;
                        username = profile.username;
                    }
                    return CellarsSDK.crm.createProfile(type, username, profile);
                },
                searchProfiles: function (query, options) {
                    return CellarsSDK.crm.searchProfiles(query, options);
                }
            };

        }];
    });

    cellars.provider('Products', function () {

        this.$get = ['CellarsSDK', function (CellarsSDK) {
            return {
                load: function (AUPID, options) {
                    return CellarsSDK.products.show(AUPID, { query: options });
                }
            };
        }];
    });

    cellars.provider('Community', function () {

        this.$get = ['CellarsSDK', function (CellarsSDK) {
            return {
                searchEvents: function (query, options) {
                    return CellarsSDK.community.search('event', query, options);
                },
                getEvent: function (key, options) {
                    return CellarsSDK.community.load('event', key, options);
                },
                listEvents: function () {
                    return CellarsSDK.community.list('event');
                },
                listEventTypes: function () {
                    return CellarsSDK.community.list('event-types');
                },
                createProductReview: function (AUPID, review) {
                    if (!review) {
                        throw new Error('invalid review');
                    }
                    review.AUPID = AUPID;
                    return CellarsSDK.community.create('productReview', review);
                },
                listProductReviews: function (AUPID, options) {
                    return CellarsSDK.community.list('productReview', AUPID, options);
                },
                removeProductReview: function (AUPID, reviewKey) {
                    return CellarsSDK.community.remove('productReview', reviewKey, AUPID);
                },
                updateProductReview: function (reviewKey, data) {
                    if (!data && !data.AUPID) {
                        throw new Error('Invalid product review');
                    }
                    console.log('Update product review', reviewKey);
                    return CellarsSDK.community.update('productReview', reviewKey, data);
                },
                loadProductReview: function (AUPID, reviewKey, options) {
                    return CellarsSDK.community.load('productReview', reviewKey, AUPID, options);
                },
                follow: function (username) {
                    return CellarsSDK.community.follow('me', [{
                        type: 'social',
                        followedUsername: username
                    }]);
                },
                unfollow: function (username) {
                    return CellarsSDK.community.unfollow('me', username);
                },
                rateProductReview: function (AUPID, reviewKey, rating) {
                    return CellarsSDK.community.rateProductReview(AUPID, reviewKey, rating);
                },
                searchProductsInEvent: function (eventKey, identity, options) {
                    return CellarsSDK.community.searchProductsInEvent(eventKey, identity, options);
                },
                searchProductReviews: function (query, options) {
                    return CellarsSDK.community.search('productReview', query, options);
                }
            };
        }];
    });

    cellars.provider('Inventory', function () {

        this.$get = ['CellarsSDK', function (CellarsSDK) {
            return {
                checkin: function (locationKey, bottles) {
                    return CellarsSDK.inventory.checkin(locationKey, bottles);
                },
                checkout: function (locationKey, bottles) {
                    return CellarsSDK.inventory.checkout(locationKey, bottles);
                },
                confirmBottles: function (locationKey, bottles) {
                    return CellarsSDK.inventory.confirmBottles(locationKey, bottles);
                },
                createLocation: function (loc, options) {
                    options = { query: options };
                    return CellarsSDK.inventory.createLocation(loc, options);
                },
                free: function (locationKey, bottles) {
                    return CellarsSDK.inventory.free(locationKey, bottles);
                },
                getLocation: function (key, options) {
                    options = { query: _.merge(options || {}, { hierarchy: true, capacity: true }) };
                    return CellarsSDK.inventory.getLocation(key, options).then(function (result) {
                        if (result) {
                            return result.data;
                        }
                    });
                },
                importBottles: function (location, ownerInfo, bottles, options) {
                    options = { query: options };
                    return CellarsSDK.inventory.importBottles(location, ownerInfo, bottles, options);
                },
                importFromLegacy: function (cellar, options) {
                    options = { query: options };
                    return CellarsSDK.inventory.importFromLegacy(cellar, options);
                },
                listBottles: function (locationKey, options) {
                    return CellarsSDK.inventory.listBottles(locationKey, options);
                },
                listLocations: function (parentKey, options) {
                    options = { query: options };
                    return CellarsSDK.inventory.listLocations(parentKey, options).then(function (result) {
                        return result.data;
                    });
                },
                listTransactions: function (options) {
                    return CellarsSDK.inventory.listTransactions(options).then(function (result) {
                        return result.data;
                    });
                },
                removeLocation: function (loc, options) {
                    return CellarsSDK.inventory.removeLocation(loc, options);
                },
                reserve: function (locationKey, bottles) {
                    return CellarsSDK.inventory.reserve(locationKey, bottles);
                },
                searchBottles: function (query, options) {
                    options = { query: options };
                    return CellarsSDK.inventory.searchBottles(query, options);
                },
                searchLocation: function (keywords, options) {
                    options = { query: options };
                    return CellarsSDK.inventory.searchLocation(keywords, options);
                },
                updateLocation: function (loc, options) {
                    options = { query: options };
                    return CellarsSDK.inventory.updateLocation(loc, options);
                },
                changeProduct: function (bottleKey, newAUPID) {
                    return CellarsSDK.inventory.changeProduct(bottleKey, newAUPID);
                },
                canReinsert: function (acode) {
                    return CellarsSDK.inventory.canReinsert(acode);
                },
                listBreadCrumbs: function (key, options) {
                    return CellarsSDK.inventory.listBreadCrumbs(key, options);
                }
            };

        }];
    });

    cellars.provider('Marketplace', function () {
        this.$get = ['CellarsSDK', function (CellarsSDK) {
            return {
                listFavoriteStores: function () {
                    return CellarsSDK.marketplace.listFavoriteStores();
                },
                listNearbyStores: function (latitude, longitude, max_distance, limit) {
                    return CellarsSDK.marketplace.listNearbyStores({
                        latitude: latitude,
                        longitude: longitude,
                        max_distance: max_distance,
                        limit: limit
                    });
                },
                toggleFavoriteStore: function (storeKey) {
                    return CellarsSDK.marketplace.toggleFavoriteStore(storeKey);
                },
                searchStores: function (query) {
                    return CellarsSDK.marketplace.searchStores(query);
                },
                searchProducts: function (query) {
                    return CellarsSDK.marketplace.searchProducts(query);
                },
                pushBottle: function (acode, qualityImage) {
                    return CellarsSDK.marketplace.pushBottle(acode, qualityImage);
                },
                popBottle: function (acode) {
                    return CellarsSDK.marketplace.popBottle(acode);
                },
                soldBottle: function (acode, data) {
                    var _soldData = data || {};
                    return CellarsSDK.marketplace.soldBottle(acode, _soldData);
                },
                listBottles: function (data) {
                    var _query = data || {};
                    return CellarsSDK.marketplace.listBottle(_query);
                }
            };
        }];
    });

})(angular);
