var gulp = require('gulp');
var bump = require('gulp-bump');
var ngAnnotate = require('gulp-ng-annotate');

/**
 * Bumping version number and tagging the repository with it.
 * Please read http://semver.org/
 *
 * You can use the commands
 *
 *     gulp patch     # makes v0.1.0 → v0.1.1
 *     gulp feature   # makes v0.1.1 → v0.2.0
 *     gulp release   # makes v0.2.1 → v1.0.0
 *
 * To bump the version numbers accordingly after you did a patch,
 * introduced a feature or made a backwards-incompatible release.
 */

function incrementVersion(importance) {
    // get all the files to bump version in
    return gulp.src(['./package.json', './bower.json'])
        // bump the version number in those files
        .pipe(bump({ type: importance }))
        // save it back to filesystem
        .pipe(gulp.dest('./'));
}


gulp.task('default', function () {
    incrementVersion('patch');
    return gulp.src('src/angular-cellars.js')
        .pipe(ngAnnotate({
            remove: true,
            add: true,
            single_quotes: true
        }))
        .pipe(gulp.dest('dist'));
});
