
describe('CellarsSDK', function(){


    describe('At runtime', function() {
        beforeEach(module('cellars'));

        var $CellarsSDK;

        beforeEach(inject(function(CellarsSDK) {
            $CellarsSDK = CellarsSDK;
        }));

        it('should expose a default base_url and api_version', function() {
            expect($CellarsSDK.base_url).to.equal('https://api.cellars.io');
            expect($CellarsSDK.api_version).to.equal('1.0');
        });

    });

    describe('Provider', function() {
        var $CellarsSDKProvider;

        beforeEach(function() {
            angular.module('testModule', ['cellars'], function(){})
                .config(['CellarsSDKProvider', function(CellarsSDKProvider) {
                    $CellarsSDKProvider = CellarsSDKProvider;
                }]);

            module('testModule');
            inject(function(){});
        });

        describe('With different base_url and api_version', function() {
            var $CellarsSDK;

            beforeEach(function() {
                console.log("Overriding provider values");
                $CellarsSDKProvider.server_url = "https://staging.cellars.io";
                $CellarsSDKProvider.api_version = "1.1";

                inject(['CellarsSDK', function(CellarsSDK){
                    $CellarsSDK = CellarsSDK;
                }]);
            });

            it("should return the overrided base_url and api_version", function() {
                console.log($CellarsSDK.base_url);
                console.log($CellarsSDK.api_version);
                expect($CellarsSDK.base_url).to.equal("https://staging.cellars.io");
                expect($CellarsSDK.api_version).to.equal("1.1");
            });

        });

    });
});
